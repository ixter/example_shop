<?php /** @noinspection UnusedFunctionResultInspection */

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Random\RandomException;

class DatabaseSeeder extends Seeder
{
    /**
     * @throws RandomException
     */
    public function run(): void
    {
        Brand::factory(20)->create();
//        Category::factory(20)->create();
        Product::factory(20)->has(Category::factory(random_int(1,3)))->create();
    }
}
