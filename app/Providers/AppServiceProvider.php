<?php

namespace App\Providers;

use App\Http\Kernel;
use Carbon\CarbonInterval;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Model::shouldBeStrict(!app()->isProduction());
        if (app()->isProduction()) {
//        DB::whenQueryingForLongerThan(500, static function (Connection $connection) {
//            logger()->channel('telegram')->debug('whenQueryingForLongerThan: ' . $connection->totalQueryDuration());
//        });
            DB::listen(static function ($query) {
//            $query->sql;
//            $query->bindings;
//            $query->time;
                if ($query->time > 1000) {
                    logger()->channel('telegram')->debug('Query longer than 10ms: ' . $query->sql, $query->bindings);
                }
//
//            dump($query->sql);
            });

            app(Kernel::class)->whenRequestLifecycleIsLongerThan(
                CarbonInterval::seconds(4),
                static function () {
                    logger()->channel('telegram')->debug('whenRequestLifecycleIsLongerThan: ' . request()->url());
                }
            );
        }
    }
}
