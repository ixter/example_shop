<?php

declare(strict_types=1);

namespace App\Traits\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Stringable;

trait HasSlug
{
    protected static function bootHasSlug(): void
    {
        static::creating(function (Model $model) {
            $model->slug = $model->slug ?? (string)self::checkSlug($model, str($model->{self::slugFrom()}));
        });
    }

    public static function checkSlug(Model $model, Stringable $slug): Stringable
    {
        $check = true;
        while ($check) {
            if (self::where('slug', $slug)->first() !== null) {
                        $slug = $slug->append('~');
            }else {
                $check = false;
            }
        }
        return $slug;
    }

    public static function slugFrom(): string
    {
        return 'title';
    }
}
