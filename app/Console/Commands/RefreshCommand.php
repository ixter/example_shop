<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class RefreshCommand extends Command
{
    protected $signature = 'shop:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh';

    /**
     * Execute the console command.
     */
    public function handle(): Int
    {
        if (app()->isProduction()){
            return self::FAILURE;
        }
        Storage::deleteDirectory('images/products');
        Storage::makeDirectory('images/products');
        $this->call('migrate:fresh', [
            '--seed' => true
            ]);
        return self::SUCCESS;


    }
}
