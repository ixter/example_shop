<?php

declare(strict_types=1);

namespace App\Logging\Telegram;

use App\Services\Telegram\TelegramBotApi;
use JetBrains\PhpStorm\NoReturn;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Monolog\LogRecord;

final class TelegramLoggerHandler extends AbstractProcessingHandler
{
    protected int $chatId;
    protected string $token;
    public function __construct( array $config)
    {
        $level = Logger::toMonologLevel($config['level']);
        parent::__construct($level);
        $this->chatId = (int)$config['chat_id'];
        $this->token = $config['token'];
//        $this->chatId = config('logging.channels.telegram.chat_id');
//        $this->token = config('logging.channels.telegram.token');
    }

    #[NoReturn] protected function write(LogRecord $record): void
    {
        TelegramBotApi::sendMessage(token: $this->token, chatId: $this->chatId, text: $record['formatted']);
//        $record['formatted']
    }
}
